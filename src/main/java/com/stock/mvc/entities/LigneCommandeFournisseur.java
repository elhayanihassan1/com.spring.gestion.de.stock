package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ligne_commande_fournisseur")
public class LigneCommandeFournisseur  implements Serializable{


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id_ligne_commande_fournisseur")
	private Long IdLigneCommandeFournisseur;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;

	
	@ManyToOne
	@JoinColumn(name = "idCommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;
	
	
	public LigneCommandeFournisseur() {
		super();
		
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}
	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
	public Long getIdLigneCommandeFournisseur() {
		return IdLigneCommandeFournisseur;
	}
	public void setIdLigneCommandeFournisseur(Long idLigneCommandeFournisseur) {
		IdLigneCommandeFournisseur = idLigneCommandeFournisseur;
	}
	

}