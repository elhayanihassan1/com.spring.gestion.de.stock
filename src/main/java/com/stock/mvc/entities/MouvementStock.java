package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "mouvement_stock")
public class MouvementStock  implements Serializable{


	private static final long serialVersionUID = 1L;
	
	
	public static final int ENTREE=1;
	public static final int SORTIE=2;
	
	@Id
	@GeneratedValue
	@Column(name = "id_mouvement_stock")
	private Long idMouvementStock;
	
	@Temporal(TemporalType.DATE)
	private Date dateMvt;
	
	private BigDecimal quantite;
	
	private int typeMvt;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	
	public MouvementStock() {
		super();
		
	}
	public Date getDateMvt() {
		return dateMvt;
	}
	public void setDateMvt(Date dateMvt) {
		this.dateMvt = dateMvt;
	}
	public BigDecimal getQuantite() {
		return quantite;
	}
	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}
	public int getTypeMvt() {
		return typeMvt;
	}
	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public static int getEntree() {
		return ENTREE;
	}
	public static int getSortie() {
		return SORTIE;
	}
	public Long getIdMouvementStock() {
		return idMouvementStock;
	}
	public void setIdMouvementStock(Long idMouvementStock) {
		this.idMouvementStock = idMouvementStock;
	}
	
	
    


}