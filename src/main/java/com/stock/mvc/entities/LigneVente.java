package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ligne_vente")
public class LigneVente  implements Serializable{


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id_ligne_vente")
	private Long iDLigneVente;
	
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;

	
	@ManyToOne
	@JoinColumn(name = "idVente")
	private Vente vente;
	

	public LigneVente() {
		super();
		
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public Vente getVente() {
		return vente;
	}
	public void setVente(Vente vente) {
		this.vente = vente;
	}
	public Long getiDLigneVente() {
		return iDLigneVente;
	}
	public void setiDLigneVente(Long iDLigneVente) {
		this.iDLigneVente = iDLigneVente;
	}
	

}