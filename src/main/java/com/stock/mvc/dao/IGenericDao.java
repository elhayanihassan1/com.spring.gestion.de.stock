package com.stock.mvc.dao;

import java.util.List;

public interface IGenericDao<E> {
	
	public E save ( E entity);
	
	public E update ( E entity);
	
	public List<E> selectALL();
	
	public List<E> selectALL( String sortField , String sort);
	
	public E getById( long id);
	
	public void remove( Long id);
	
	public E findOne( String paramName, Object paramValue);
	
	public E findOne( String[] paramNames, Object[] paramValues);
	
	public int findCountBy( String paramName, Object paramValue);



}
