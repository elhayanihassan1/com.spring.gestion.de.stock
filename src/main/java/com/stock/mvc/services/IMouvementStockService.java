package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.MouvementStock;

public interface IMouvementStockService {

	public MouvementStock save(MouvementStock entity);

	public MouvementStock update(MouvementStock entity);

	public List<MouvementStock> selectALL();

	public List<MouvementStock> selectALL(String sortField, String sort);

	public MouvementStock getById(long id);

	public void remove(Long id);

	public MouvementStock findOne(String paramName, Object paramValue);

	public MouvementStock findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, Object paramValue);

}
