package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IArticleDao;
import com.stock.mvc.entities.Article;
import com.stock.mvc.services.IArticleService;

@Transactional
public class ArticleServiceImpl implements IArticleService {
	
	
	private IArticleDao dao;
	

	@Override
	public Article save(Article entity) {
		return dao.save(entity);
	}

	@Override
	public Article update(Article entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Article> selectALL() {
	
		return dao.selectALL();
	}

	@Override
	public List<Article> selectALL(String sortField, String sort) {
	
		return dao.selectALL(sortField, sort);
	}

	@Override
	public Article getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
          dao.remove(id);
	}

	@Override
	public Article findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Article findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
	
		return dao.findCountBy(paramName, paramValue);
	}
	
	public IArticleDao getDao() {
		return dao;
	}

	public void setDao(IArticleDao dao) {
		this.dao = dao;
	}

}
