package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ICategorieDao;
import com.stock.mvc.entities.Categorie;
import com.stock.mvc.services.ICategorieService;



@Transactional
public class CategorieServiceImpl implements ICategorieService {
	
	
private ICategorieDao dao;
	

	@Override
	public Categorie save(Categorie entity) {
		return dao.save(entity);
	}

	@Override
	public Categorie update(Categorie entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Categorie> selectALL() {
	
		return dao.selectALL();
	}

	@Override
	public List<Categorie> selectALL(String sortField, String sort) {
	
		return dao.selectALL(sortField, sort);
	}

	@Override
	public Categorie getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
          dao.remove(id);
	}

	@Override
	public Categorie findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Categorie findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
	
		return dao.findCountBy(paramName, paramValue);
	}
	
	public ICategorieDao getDao() {
		return dao;
	}

	public void setDao(ICategorieDao dao) {
		this.dao = dao;
	}



}
