package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.Vente;

public interface IVenteService {

	public Vente save(Vente entity);

	public Vente update(Vente entity);

	public List<Vente> selectALL();

	public List<Vente> selectALL(String sortField, String sort);

	public Vente getById(long id);

	public void remove(Long id);

	public Vente findOne(String paramName, Object paramValue);

	public Vente findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, Object paramValue);

}
