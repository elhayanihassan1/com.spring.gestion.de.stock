package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.LigneCommandeClient;

public interface ILigneCommandeClientService {

	public LigneCommandeClient save(LigneCommandeClient entity);

	public LigneCommandeClient update(LigneCommandeClient entity);

	public List<LigneCommandeClient> selectALL();

	public List<LigneCommandeClient> selectALL(String sortField, String sort);

	public LigneCommandeClient getById(long id);

	public void remove(Long id);

	public LigneCommandeClient findOne(String paramName, Object paramValue);

	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues);

	public int findCountBy(String paramName, Object paramValue);

}
