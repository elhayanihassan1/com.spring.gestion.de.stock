    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="javascript:void(0);">
        <div class="sidebar-brand-icon rotate-n-15">
         <i class="fa fa-shopping-cart" style="font-size:48px;color:red"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Gestion de stock</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <c:url value="/home/" var="home" />
        <a class="nav-link" href="${home}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span> <fmt:message key="common.dashbord" />  </span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Nav Item - Article Collapse Menu -->
           <li class="nav-item">
           <c:url value="/article/" var="article" />
        <a class="nav-link" href="${article}">
          <i class="fa fa-shopping-cart"></i>
          <span> <fmt:message key="common.article" />  </span></a>
      </li>


      <!-- Nav Item - Client Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-users"></i>
          <span><fmt:message key="common.client" /></span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <c:url value="/client/" var="client" />
            <a class="collapse-item" href="${client}"><fmt:message key="common.client" /></a>
           <c:url value="/commandeclient/" var="cdeClient" />
            <a class="collapse-item" href="${cdeClient}"><fmt:message key="common.client.commande" /></a>
          </div>
        </div>
      </li>
       
       
      <!-- Nav Item - Fournisseur Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseComponents" aria-expanded="true" aria-controls="collapseComponents">
          <i class="fas fa-user-tie"></i>
          <span><fmt:message key="common.fournisseur" /></span>
        </a>
        <div id="collapseComponents" class="collapse" aria-labelledby="heading" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <c:url value="/fournisseur/" var="fournisseur" />
            <a class="collapse-item" href="${fournisseur}"><fmt:message key="common.fournisseur" /></a>
          <c:url value="/commandefournisseur/" var="cdeFournisseur" />
            <a class="collapse-item" href="${cdeFournisseur}"><fmt:message key="common.fournisseur.commande" /></a>
          </div>
        </div>
      </li>
      
      <!-- Divider -->
      <hr class="sidebar-divider">

      
            <!-- Nav Item - Stock -->
      <li class="nav-item">
       <c:url value="/stock/" var="stock" />
        <a class="nav-link" href="${stock}">
          <i class="fas fa-fw fa-table"></i>
          <span><fmt:message key="common.stock" /></span></a>
      </li>
      
      
      <!-- Nav Item - Ventes -->
      <li class="nav-item">
      <c:url value="/vente/" var="vente" />
        <a class="nav-link" href="${vente}">
          <i class="fas fa-fw fa-chart-area"></i>
          <span><fmt:message key="common.vente" /></span></a>
      </li>
      
      <!-- Nav Item - Parametrages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link  collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-wrench"></i>
          <span><fmt:message key="common.parametrage" /></span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <c:url value="/utilisateur/" var="user" />
            <a class="collapse-item" href="${user}"><fmt:message key="common.parametrage.utilisateur" /></a>
          <c:url value="/categorie/" var="categorie" />
            <a class="collapse-item" href="${categorie}"><fmt:message key="common.parametrage.categorie" /></a>
          </div>
        </div>
      </li>



      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->